/**
 * 对数据库操作的类
 * 对数据库的操作，就是crud
 * 调用存储过程
 */
package com.db;

import java.sql.*;

import com.mytools.Auto_close_mes;
import com.mytools.Read_ini;

/**
 * @author 火蚁
 * 
 * @类名 SqlHelper
 * 
 * @成员方法 
 * 			{@link #SqlHelper()}	构造函数<br>
 * 			{@link #query(String, String[])} 查询方法<br>
 * 			{@link #update(String, String[])} 更新表数据<br>
 * 			{@link #close()} 关闭数据库资源
 * 
 * @成员变量 
 * 			{@link #conn} 数据库连接接口<br>
 * 			{@link #ps} 预编译的 SQL 语句的对象<br>
 * 			{@link #rs} 查询返回的结果集<br>
 * 			{@link #file_path} 连接数据库配置文件的路径<br>
 * 			{@link #setion} 配置文件中的节点【节点名】<br>
 * 			{@link #db_type} 数据库的类型<br>
 * 			{@link #driver} 连接数据库的驱动名<br>
 * 			{@link #host} 数据库主机IP地址<br>
 * 			{@link #db_name} 该系统数据库的名称<br>
 * 			{@link #url} 给定数据库 URL 的连接<br>
 * 
 * @version 2013-8-22
 * 
 * @修改日志 
 * 			2013-08-24<br>
 * 			1. 加入了如果数据库服务没有开启，则给出提示信息在登录界面中<br>
 * 			2. 需要改进的地方，应当直到用户开启了数据库的服务才放行登录界面的操作
 * 
 */
public class SqlHelper {
	// 定义需要的对象
	Connection conn = null;
	PreparedStatement ps = null;
	ResultSet rs = null;
	
	// 从数据库配置文件db.ini文件中读取数据库的配置信息
	// 配置文件的路径
	String file_path = "./config/db.ini";
	// 节点
	String setion = "db_config";
	// 数据库的类型
	String db_type = Read_ini.get_pro_str(file_path, setion, "db_type", "sql server");

	// 连接数据库需要的字符串(driver,url,user,password)
	String driver = null;
	
	// 主机地址
	String host = Read_ini.get_pro_str(file_path, setion, "host", "127.0.0.1");
	// 数据库的名称
	String db_name = Read_ini.get_pro_str(file_path, setion, "db_name", "");
	String url = "jdbc:sqlserver://"+host+";databaseName="+db_name;
	
	// 用户名
	String user = Read_ini.get_pro_str(file_path, setion, "user", "root");
	
	// 密码
	String password = Read_ini.get_pro_str(file_path, setion, "password", "");
	
	/**
	 * SqlHelper构造方法 {@link #SqlHelper()}
	 *  
	 *@param 
	 *		{@link #conn} 数据库连接接口<br>
	 * 		{@link #ps} 预编译的 SQL 语句的对象<br>
	 * 		{@link #rs} 查询返回的结果集<br>
	 * 		{@link #file_path} 连接数据库配置文件的路径<br>
	 * 		{@link #setion} 配置文件中的节点【节点名】<br>
	 * 		{@link #db_type} 数据库的类型<br>
	 * 		{@link #driver} 连接数据库的驱动名<br>
	 * 		{@link #host} 数据库主机IP地址<br>
	 * 		{@link #db_name} 该系统数据库的名称<br>
	 * 		{@link #url} 给定数据库 URL 的连接<br>
	 * 
	 */
	public SqlHelper() {
		
		
		if (db_type.equals("sql server")) {
			
			driver = "com.microsoft.jdbc.sqlserver.SQLServerDriver";
		}
		
		try {
			// 加载驱动
			Class.forName(driver);
			// 得到连接
			conn = DriverManager.getConnection(url, user, password);
			
		} catch (ClassNotFoundException e) {
			
			new Auto_close_mes("数据库驱动加载失败", 400, 150, 30);
			e.printStackTrace();
			
		} catch (NullPointerException e) {
			
			e.printStackTrace();
			
		} catch (SQLException e) {
			
			new Auto_close_mes("<html><font color = 'red'>数据库服务没有启动<br><br>请启动系统的数据库服务，再登录", 400, 290, 80);
			return;
		} 
	}

	/**
	 * 数据库查询方法
	 * 
	 * @param sql sql查询语句
	 * 
	 * @param paras 查询条件数组，可以防止注入漏洞
	 * 
	 * @return rs ResultSet结果集
	 */
	public ResultSet query(String sql, String[] paras) {
		try {
			ps = conn.prepareStatement(sql);
			// 对sql的参数赋值
			for (int i = 0; i < paras.length; i++) {
				ps.setString(i + 1, paras[i]);
			}
			// 执行查询
			rs = ps.executeQuery();
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		// 返回结果集
		return rs;
	}
	
	/**
	 * 对数据表的增删改操作方法
	 * 
	 * @param sql sql语句
	 * 
	 * @param paras 条件数组
	 * 
	 * @return ture 表示执行成功<br/> false 表示执行失败
	 */
	public boolean update(String sql, String[] paras) {
		
		boolean b = true;
		try {

			ps = conn.prepareStatement(sql);
			// 循环的对paras赋值，？赋值法
			for (int i = 0; i < paras.length; i++) {
				ps.setString(i+1, paras[i]);
			}
			// 执行操作
			ps.execute();
			
		} catch (Exception e) {
			
			e.printStackTrace();
			b = false;
		}
		return b;
	} 
	
	/**
	 * 关闭数据库各种资源
	 * 
	 * @return void
	 * 
	 */
	public void close() {
		try {
			if (rs != null) {
				rs.close();
			}

			if (ps != null) {
				ps.close();
			}

			if (conn != null) {
				conn.close();
			}

		} catch (Exception e) {

			e.printStackTrace();
		}
	}
}
