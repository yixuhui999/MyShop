package com.mytools;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;

import com.sun.awt.AWTUtilities;

/**
 * 用于创建一个自动关闭的消息提示对话框
 * 
 * @author 火蚁
 *
 */
@SuppressWarnings("serial")
public class Auto_close_mes extends JDialog{
	
	/**
	 * 构造函数
	 * 
	 * @param text 提示文本内容
	 * @param x 对话框宽度
	 * @param y 对话框高度
	 * @param delay_time 自动关闭的时间
	 */
	public Auto_close_mes(String text, int x, int y, int delay_time) {
		
		//new Auto_close_mes("员工编号只能由数字组成，请更正！", 400, 150, 15);
		auto_close(this, delay_time);
		JLabel con = new JLabel(text, JLabel.CENTER);
		con.setForeground(Color.blue);
		con.setFont(new Font("SimSun", Font.BOLD, 20));
		
		JPanel bk = new JPanel(new GridLayout(1,1));
		bk.setBorder(BorderFactory.createEtchedBorder());
		bk.add(con);
		bk.setBackground(new Color(214,227,238));
		
		// 设置对话框为总在顶层
		this.setAlwaysOnTop(true);
		
		this.setUndecorated(true);
		AWTUtilities.setWindowOpacity(this, 0.75f);
		this.add(bk);
		this.setSize(x, y);
		this.setVisible(true);
		//this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		
	}
	
	/**
	 * 对话框自动关闭的实现方法
	 * 
	 * @param win 对话框对象实例
	 * @param close_time 自动关闭的时间
	 * 
	 */
	public void auto_close(final JDialog win, final int close_time) {
		
		ActionListener lisener = new ActionListener() {
			
			int t = 0;
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				if(t < close_time) {
					
					t++;
				}else {
					
					win.dispose();
					Timer source = (Timer) e.getSource();
					// 停止计时器
					source.stop();
				}
			}
		};
		// 启动计时器
		new Timer(120, lisener).start();
	}
}
