package com.mytools;

import java.awt.Font;

/**
 * 公用字体库，用于统一同一类的标签的字体样式
 * 
 * @version 2013-07-20
 * 
 * @成员变量
 * 		TopMenu 顶层菜单栏字体样式<br>
 * 		TopMenuC 顶层菜单鼠标经过样式<br>
 * 		Infolab 标签样式<br>
 * 		PaddInfotext 产品添加标签样式<br>
 * 		Infotext 输入文本的样式<br>
 * 		login 登录窗口字体样式
 *
 */
public class MyFont {
	
	public static Font TopMenu=new Font("新宋体",Font.BOLD,18);
	public static Font TopMenuC=new Font("新宋体",Font.BOLD,21);
	public static Font Infolab=new Font("新宋体",Font.BOLD,22);
	public static Font PaddInfotext=new Font("新宋体",Font.PLAIN,22);
	public static Font Infotext=new Font("新宋体",Font.BOLD,20);
	public static Font login=new Font("宋体",Font.PLAIN,15);
}
