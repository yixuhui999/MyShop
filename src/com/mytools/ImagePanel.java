package com.mytools;

import java.awt.*;
import javax.swing.*;

/**
 * 用于动态生成一个图片面板，图片的大小自适应面板的实际大小
 * 
 * @version 2013-06-30
 * 
 * @成员变量 
 * 		im 面板需要的背景图片
 * 
 * @成员方法
 * 		{@link #ImagePanel(Image im)} 图片面板的构造函数<br>
 * 		{@link #paintComponent(Graphics)} 用于画出背景图片
 *
 */
@SuppressWarnings("serial")
public class ImagePanel extends JPanel{
	
	Image im;
	
	/**
	 * 图片面板构造函数，指定图片面板的大小
	 * 
	 * @param im 面板需要的背景图片
	 */
	public ImagePanel(Image im) {
		
		this.im=im;//此处的this不能少，否则不能画出图片
		//希望它大小是自我调整的
		int width=Toolkit.getDefaultToolkit().getScreenSize().width;
		int height=Toolkit.getDefaultToolkit().getScreenSize().height;
		this.setSize(width,height);
	}
	
	//画出背景
	/**
	 * 在面板上画出图片
	 * 
	 * @param g 画图片需要的画笔
	 */
	public void paintComponent(Graphics g) {
		
		//清屏
		super.paintComponent(g);
		g.drawImage(im,0,0,this.getWidth(),this.getHeight(),this);
		
	}
}
