package com.model;

import java.util.*;

/**
 * 操作库存表的数据模型
 * 
 * @version 2013-07-26
 * 
 * @成员方法 
 * 		{@link #query(String, String[])} 查询数据
 *
 */
@SuppressWarnings("serial")
public class LookStcokModel extends BaseModel {

	/**
	 * 数据查询方法
	 */
	@SuppressWarnings("rawtypes")
	public void query(String sql, String paras[]) {
		
		this.colums=new Vector<String>();
		this.rows = new Vector<Vector>();
		
		String[] colum_name = {"产品编号", "产品名称", "产品类别", "库存数量"};
		// 将列名数组加入到列向量中
		for (int i = 0; i < colum_name.length; i++) {
			
			this.colums.add(colum_name[i]);
		}
		super.query(sql, paras);
	}
}