/**
 *  基本表格模型类
 *  日期：2013-8-22
 *  作用：将公用的表格模型抽象出来，减少代码的冗余
 */

package com.model;

import com.db.*;

import javax.swing.table.*;
import java.util.*;
import java.sql.*;

/**
 * 
 * 基本表格模型类
 * 
 * @version 
 * 		2013-08-22
 * 
 * @作用 
 * 		将公用的表格模型抽象出来作为大部分model类的父类，减少代码冗余，提高代码的复用性
 * 
 * @成员变量 
 * 		{@link #colums} 
 * 			用于存放表格每一列的向量<br>
 * 		{@link #rows} 
 * 			用于存放表格的每一行
 * 
 * @成员方法
 * 		{@link #query(String, String[])} 
 * 			查询方法，结果集来源与 {@link #SqlHelper()}的{@link #query(String, String[])}<br>
 * 
 * 		{@link #getColumnName(int)} 
 * 			得到列名<br>
 * 
 * 		{@link #getRowCount()} 
 * 			得到总行数<br>
 * 
 * 		{@link #getColumnCount()} 
 * 			得到总列数<br>
 * 
 * 		{@link #getValueAt(int, int)} 
 * 			得到表格中指定位置的值<br>
 *
 */
public class BaseModel extends AbstractTableModel {
	
	
	private static final long serialVersionUID = -8366628798040563794L;
	
	/**
	 * 表示表格的列
	 */
	protected Vector<String> colums;
	/**
	 * 表示表格的行
	 */
	@SuppressWarnings("rawtypes")
	protected Vector<Vector> rows;// 行
	

	/**
	 * 数据查询方法
	 * 
	 * @param sql sql语句
	 * 
	 * @param paras 条件数组
	 * 
	 */
	protected void query(String sql, String paras[]) {
		
		// 创建sqlhelper对象
		SqlHelper sh = new SqlHelper();
		ResultSet rs = sh.query(sql, paras);
		try {

			// 从rs对象中可以得到一个ResultSetMetaData
			// rsmt可以的到结果有多少列，而且可以知道每列的名字，加入表头的信息
			ResultSetMetaData rsmt = rs.getMetaData();
			// 把rs的结果放入到rows
			while (rs.next()) {

				Vector<String> temp = new Vector<String>();
				for (int i = 0; i < rsmt.getColumnCount(); i++) {
					temp.add(rs.getString(i + 1));
				}
				rows.add(temp);
			}
		} catch (Exception e) {
			
			e.printStackTrace();
			
		} finally {
			
			sh.close();
		}	
	}
	
	/**
	 * 获得每一列的名字，如果没有这个方法则默认列名是A->Z
	 * 
	 */
	public String getColumnName(int column) {
		// TODO Auto-generated method stub
		return colums.get(column);
	}
	
	/**
	 * 得到总的行数
	 */
	public int getRowCount() {
		// TODO Auto-generated method stub
		return rows.size();
	}
	
	/**
	 * 得到总的列数
	 */
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return colums.size();
	}
	
	/**
	 * 得到表格中指定位置的值
	 */
	@SuppressWarnings("rawtypes")
	public Object getValueAt(int rowIndex, int columnIndex) {
		// TODO Auto-generated method stub
		return ((Vector) rows.get(rowIndex)).get(columnIndex);
	}
}
