package com.model;

import com.db.*;

import java.util.*;
import java.sql.*;

/**
 * 会员表格模型，完成对雇员表的增删改查操作，提供会员表格模型
 * 
 * @version 2013-08-22
 * 
 * @成员方法 
 * 		{@link #query(String, String[])} 数据查询<br>
 * 		{@link #Memberupdate(String, String[])} 对会员信息表数据进行更新操作<br>
 * 		{@link #checkid(String)} 检查会员ID是否已经存在	
 *
 */
@SuppressWarnings("serial")
public class MemberModel extends BaseModel {
	
	/**
	 * 查询方法
	 */
	@SuppressWarnings("rawtypes")
	public void query(String sql, String paras[]) {
		
		this.colums=new Vector<String>();
		this.rows = new Vector<Vector>();
		
		String[] colum_name = {"会员编号", "姓        名", "性        别", "年        龄", "完美卡号"};
		// 将列名数组加入到列向量中
		for (int i = 0; i < colum_name.length; i++) {
			
			this.colums.add(colum_name[i]);
			
		}		
		super.query(sql, paras);	
	}
	
	/**
	 * 
	 * @param sql sql语句
	 * 
	 * @param paras 条件数组
	 * 
	 * @return
	 * 		true 更新成功<br>
	 * 		false 更新失败
	 */
	public boolean Memberupdate(String sql, String[] paras) {
		
		boolean b = false;
		SqlHelper sh = new SqlHelper();
		b = sh.update(sql, paras);
		
		sh.close();
		
		return b;
	}
	
	/**
	 * 用于检查会员编号是否已经存在
	 * 
	 * @param id 会员编号
	 * 
	 * @return
	 * 		true 存在<br>
	 * 		false 不存在
	 */
	public boolean checkid(String id) {
		
		boolean b = false;
		String sql = "select count(*) from MemberInfo where Mid = ?";
		String []paras = {id};
		SqlHelper sh=new SqlHelper();
		try {
			
			ResultSet rs = sh.query(sql, paras);
			if(rs.next())
			{
				//如果进去，则比较
				System.out.println(rs.getInt(1));
				if (rs.getInt(1) == 1) {
					
					b = true;
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			sh.close();
		}
		return b;
	}
}
