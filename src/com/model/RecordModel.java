package com.model;

import java.util.*;

/**
 * 操作产品入库信息表的数据模型
 * 
 * @version 2013-07-25
 * 
 * @成员方法 {@link #query(String, String[])} 数据查询方法
 *
 */
@SuppressWarnings("serial")
public class RecordModel extends BaseModel {

	/**
	 * 数据查询方法
	 */
	@SuppressWarnings("rawtypes")
	public void query(String sql, String paras[]) {

		this.colums=new Vector<String>();
		this.rows = new Vector<Vector>();
		
		String[] colum_name = {"入库编号", "产品编号", "产品名称", "产品类别", "入库数量","入库日期","负  责  人"};
		// 将列名数组加入到列向量中
		for (int i = 0; i < colum_name.length; i++) {
			
			this.colums.add(colum_name[i]);
		}
		
		super.query(sql, paras);
	}
}