package com.model;

import com.db.*;

import java.util.*;
import java.sql.*;

/**
 * 对收款界面操作的数据模型
 * 
 * @version 2013-8-22
 * 
 * @成员方法 
 * 		{@link #query(String, String[])} 数据查询<br>
 * 		{@link #update(String, String[])} 数据更新<br>
 * 		{@link #check(String, String)} 用于检查添加的购买商品是否存在<br>
 * 		{@link #get_p_num(String)} 用于返回购买商品的库存数量
 * 
 * @存在漏洞问题 
 * 		1.多次添加相同产品的时候无法正确的判断库存量是否满足购买量<br>
 *
 */
@SuppressWarnings("serial")
public class ShowKuanModel extends BaseModel {
	
	/**
	 * 数据查询
	 */
	@SuppressWarnings("rawtypes")
	public void query(String sql, String paras[]) {

		this.colums=new Vector<String>();
		this.rows = new Vector<Vector>();
		
		String[] colum_name = {"商品编号", "商品名称", "价        格", "购买数量", "合计金额"};
		// 将列名数组加入到列向量中
		for (int i = 0; i < colum_name.length; i++) {
			
			this.colums.add(colum_name[i]);
		}
		
		super.query(sql, paras);
	}
	
	/**
	 * 对数据表进行增删改等更新操作
	 * 
	 * @param sql sql语句
	 * 
	 * @param paras 条件数组
	 * 
	 * @return
	 * 		true 更行成功<br>
	 * 		false 更新失败
	 */
	static public boolean update(String sql, String[] paras) {
		
		boolean b = false;
		SqlHelper sh = new SqlHelper();
		b = sh.update(sql, paras);
		
		sh.close();
		
		return b;
	}
	
	/**
	 * 用于检查添加的购买商品产品信息表中是否存在
	 * 
	 * @param sql sql语句
	 * 
	 * @param pid 条件数组
	 * 
	 * @return
	 * 		true 存在<br>
	 * 		false 不存在
	 */
	public static boolean check(String sql,String pid) {
		
		boolean b = false;
		String []paras = {pid};
		SqlHelper sh=new SqlHelper();
		try {
			
			ResultSet rs = sh.query(sql, paras);
			if(rs.next())
			{
				//如果进去，则比较
				if (rs.getInt(1) == 1) {
					
					b = true;
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
		} finally {
			sh.close();
		}
		return b;
	}
	
	/**
	 * 用于返回购买商品的库存数量，检查库存是否满足购买需求量
	 * 
	 * @param pid 产品ID
	 * 
	 * @return
	 * 		指定产品ID的库存数量
	 */
	public static int get_p_num(String pid) {
		
		int num = 0;
		
		String []paras = {pid};
		String sql = "select sum(Num) from Stcok where Pid = ?";
		SqlHelper sh=new SqlHelper();
		try {
			
			ResultSet rs = sh.query(sql, paras);
			if(rs.next())
			{
				num = rs.getInt(1);
			}			
		} catch (Exception e) {
			
			//e.printStackTrace();
		} finally {
			sh.close();
		}
		
		return num;
	}
}
