package com.model;

import java.sql.ResultSet;
import java.util.Vector;

import com.db.SqlHelper;

/**
 * 对用户登录表操作的数据模型
 * 
 * @version 2013-07-20
 * 
 * @成员方法 
 * 		{@link #find(String, String[])} 用于取出登录表中存在的登录ID<br>
 * 		{@link #checkid(String)} 检查登录表中是否存在指定登录ID<br>
 * 		{@link #checkpassword(String, String)} 检查用户登录密码是否正确
 *
 */
public class LoginModel {
	
	
	/**
	 * 取出登录表中存在的登录ID
	 * 
	 * @param sql sql语句
	 * 
	 * @param paras 条件数组
	 * 
	 * @return
	 * 		由登录表中登录ID组成的字符串向量
	 */
	public static Vector<String> find(String sql, String[] paras) {
		
		Vector<String> temp = new Vector<String>();
		SqlHelper sh=new SqlHelper();
		try {
			
			ResultSet rs = sh.query(sql, paras);
			while(rs.next())
			{
				//如果进去，则取出第一个值
				temp.add(rs.getString(1));
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			sh.close();
		}
		
		return temp;
	}
	
	/**
	 * 检查指定ID是否具有登录的权限
	 * 
	 * @param uid 用户ID
	 * 
	 * @return
	 * 		true 可以登录<br>
	 * 		false 不可以登录
	 */
	public static boolean checkid(String uid) {
		
		boolean b = false;
		SqlHelper sh = new SqlHelper();
		String[] paras = {uid};
		String sql  = "select count(*) from UserLogin where Uid = ?";
		ResultSet rs = sh.query(sql, paras);
		try {
			
			while (rs.next()) {
				
				if(rs.getInt(1) == 1) {
					
					b = true;
				}
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			
			sh.close();
		}
		return b;
	}
	
	/**
	 * 检查用户名和密码是否匹配
	 * 
	 * @param uid 用户ID
	 * 
	 * @param password 用户密码
	 * 
	 * @return 
	 * 		true 密码正确，启动系统<br>
	 * 		false 给出错误信息提示，并要求用户重新输入密码
	 */
	public static boolean checkpassword(String uid, String password) {
		
		boolean b = false;
		String[] paras = {uid};
		SqlHelper sh = new SqlHelper();
		ResultSet rs = sh.query("select Upassword from UserLogin where Uid = ?", paras);
		try {
			
			while (rs.next()) {
				
				if(rs.getString(1).equals(password)) {
					
					b = true;
				}
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		return b;
	}
}
