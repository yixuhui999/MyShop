package com.model;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.Vector;

import org.jfree.data.*;
import org.jfree.data.category.*;
import org.jfree.data.general.DatasetUtilities;

import com.db.SqlHelper;

/**
 * 销售统计报表的数据模型	
 * 
 * @version 2013-07-29
 * 
 * @成员方法 {@link #getsum(String, String, String[])} 得到指定的数据集
 *
 */
public class ReportModel {
	 
	/**
	 * 传入条件得到需要的数据集，用以完成不同需要报表数据集的创建
	 * 
	 * @param titletext 报表的标题
	 * 
	 * @param sql 创建数据集的sql语句
	 * 
	 * @param paras 创建数据集的条件数组
	 * 
	 * @return
	 * 		创建报表需要的数据集CategoryDataset
	 */
	public static CategoryDataset getsum(String titletext, String sql, String[] paras) {
		
		SqlHelper sh = new SqlHelper();

		ResultSet rs = sh.query(sql, paras);
		
		DefaultKeyedValues keyvalues = new DefaultKeyedValues();
		
		// 取出结果
		try {
			
			ResultSetMetaData rsmt = rs.getMetaData();
			while (rs.next()) {
				
				Vector<String> temp = new Vector<String>();
				for (int i = 0; i < rsmt.getColumnCount(); i++) {
					
					temp.add(rs.getString(i + 1));
				}
				keyvalues.addValue(temp.get(0), Double.valueOf(temp.get(1)));
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}finally {
			
			sh.close();
		}
		// 创建数据集实例
		CategoryDataset dateset = DatasetUtilities.createCategoryDataset(titletext, keyvalues);
		
		return dateset;
	}	
}
