package com.model;

import java.sql.ResultSet;
import java.util.Vector;

import com.db.SqlHelper;

/**
 * 产品表操作数据模型
 * 
 * @version 2013-08-22
 * 
 * @成员方法 
 * 		{@link #query(String, String[])} 数据查询<br>
 * 		{@link #update(String, String[])} 数据更新<br>
 * 		{@link #check(String, String)} 用于检查库存表中是否已经存在该产品，产品入库时用到<br>
 * 		{@link #checknum(String, String[])} 用指定来搜索产品信息，在查找产品时用到<br>
 * 		{@link #getpid(String)} 用于得到产品的ID
 *
 */
@SuppressWarnings("serial")
public class ProductModel extends BaseModel {
	
	/**
	 * 数据查询
	 * 
	 * 
	 */
	@SuppressWarnings("rawtypes")
	public void query(String sql, String paras[]) {

		this.colums=new Vector<String>();
		this.rows = new Vector<Vector>();
		
		String[] colum_name = {"产品编号", "产品名称", "价        格", "产品积分", "产品种类"};
		// 将列名数组加入到列向量中
		for (int i = 0; i < colum_name.length; i++) {
			
			this.colums.add(colum_name[i]);
		}
		
		super.query(sql, paras);
	}
	
	/**
	 * 更新产品信息表的数据
	 * 
	 * @param sql sql语句
	 * @param paras 条件数组
	 * 
	 * @return
	 * 		true 更新成功<br>
	 * 		false 更新失败
	 */
	static public boolean update(String sql, String[] paras) {
		
		boolean b = false;
		SqlHelper sh = new SqlHelper();
		b = sh.update(sql, paras);
		
		sh.close();
		
		return b;
	}
	
	/**
	 * 用于检查库存表中是否已经存在该产品，产品入库时用到
	 * @param sql sql语句
	 * 
	 * @param tiaojian 检查条件
	 * 
	 * @return
	 * 		true 库存表存在相同的产品，进行更新库存数量操作<br>
	 * 		false 库存表中不存在相同的产品，进行插入操作
	 * 
	 * @作用 用于避免在库存表中插入重复的产品库存
	 */
	public static boolean check(String sql, String tiaojian) {
		
		boolean b = false;
		String []paras = {tiaojian};
		SqlHelper sh=new SqlHelper();
		try {
			
			ResultSet rs = sh.query(sql, paras);
			if(rs.next())
			{
				//如果进去，则比较
				if (rs.getInt(1) >= 1) {
					
					b = true;
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			sh.close();
		}
		return b;
	}
	
	/**
	 * 用指定来搜索产品信息，在查找产品时用到
	 * 
	 * @param sql sql语句
	 * 
	 * @param paras 条件数组
	 * 
	 * @return
	 * 		true 在产品信息表中找到相关的产品信息<br>
	 * 		false 没有找到相关的产品信息
	 * 
	 * @作用 用于实现对产品信息表的组合查找
	 */
	public static boolean checknum(String sql, String[] paras) {
		
		boolean b = false;
		SqlHelper sh=new SqlHelper();
		try {
			
			ResultSet rs = sh.query(sql, paras);
			if(rs.next())
			{
				//如果进去，则比较
				if (rs.getInt(1) >= 1) {
					
					b = true;
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			sh.close();
		}
		return b;
	}
	
	/**
	 * 用于得到产品的ID
	 * 
	 * @param tiaojian 条件
	 * 
	 * @return
	 * 		产品的ID
	 */
	public static String getpid(String tiaojian) {
		
		String pid = null;
		String []paras = {tiaojian};
		String sql = "select Pid from ProductInfo where Pid = ?";
		SqlHelper sh=new SqlHelper();
		try {
			
			ResultSet rs = sh.query(sql, paras);
			if(rs.next())
			{
				pid = rs.getString(1);	
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			sh.close();
		}
		
		return pid;
	}
}
