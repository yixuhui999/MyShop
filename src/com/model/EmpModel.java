/**
 * 这是人事的数据模型，完成对人事表的个各种操作
 * 修改日期：2013-08-22
 * 继承于BaseModel(公用表格模型)
 */
package com.model;

import com.db.*;

import java.util.*;
import java.sql.*;

/**
 * 
 * 雇员表格模型，完成对雇员表的增删改查操作，提供表格模型数据
 * 
 * @version 
 * 		2013-08-22
 * 
 * @父类 
 * 		BaseModel(公用表格模型)
 * 
 * @成员方法
 * 		{@link #query(String, String[])} 数据查询方法<br>
 * 		{@link #Empupdate(String, String[])} 更新数据表，增删改<br>
 * 		{@link #checkid(String)} 检查雇员ID是否已经存在
 *
 */
@SuppressWarnings("serial")
public class EmpModel extends BaseModel {		

	
	/**
	 * 数据库查询
	 * 
	 *  @实参 colum_name
	 *  		 表格列名
	 */
	@SuppressWarnings("rawtypes")
	public void query(String sql, String[] paras) {
		
		this.colums=new Vector<String>();
		this.rows = new Vector<Vector>();
		
		String[] colum_name = {"员工编号", "姓        名", "性        别", "年        龄", "职        位"};
		// 将列名数组加入到列向量中
		for (int i = 0; i < colum_name.length; i++) {
			
			this.colums.add(colum_name[i]);
		}
		
		super.query(sql, paras);
	}
	
	/**
	 * 对数据库表更新
	 * 
	 * @param 
	 * 		sql sql语句
	 * 
	 * @param 
	 * 		paras 条件数组
	 * 
	 * @return 
	 * 		true 更新成功<br>
	 * 		false 更新失败
	 */
	public boolean Empupdate(String sql, String[] paras) {
		
		boolean b = false;
		SqlHelper sh = new SqlHelper();
		b = sh.update(sql, paras);
		
		sh.close();
		
		return b;
	}
	
	/**
	 * 检查雇员ID是否已经存在
	 * 
	 * @param id 雇员编号
	 * 
	 * @return
	 * 		true 存在<br>
	 * 		false 不存在
	 */
	public boolean checkid(String id) {
		
		boolean b = false;
		String sql = "select count(*) from EmployeeInfo where Eid = ?";
		String []paras = {id};
		SqlHelper sh=new SqlHelper();
		try {
			
			ResultSet rs = sh.query(sql, paras);
			if(rs.next())
			{
				//如果进去，则比较
				if (rs.getInt(1) == 1) {
					
					b = true;
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			sh.close();
		}
		return b;
	}
}
