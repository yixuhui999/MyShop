/**
 *  对销售表进行操作的数据模型
 *  日期： 2013-07-28
 *  
 *  功能说明：
 *  	
 */

package com.model;

import java.sql.ResultSet;
import java.util.Vector;

import com.db.SqlHelper;

/**
 * 对销售表进行操作的数据模型
 * 
 * @version 2013-07-28
 * 
 * @成员方法 
 * 		{@link #query(String, String[])} 数据查询<br>
 * 		{@link #check(String, String[])} 检查是否存在相关的销售信息<br>
 * 		{@link #find(String, String[])} 用于得到销售信息表存在的年份和月份
 * 
 * @功能说明
 * 		1.完成对销售表信息的查询<br>
 *  	2.可以返回统计需要的各种数据
 *
 */
@SuppressWarnings("serial")
public class SellModel  extends BaseModel{

	/**
	 * 用于查询显示指定的销售记录
	 */
	@SuppressWarnings("rawtypes")
	public void query(String sql, String paras[]) {

		this.colums=new Vector<String>();
		this.rows = new Vector<Vector>();
		
		String[] colum_name = {"销售编号", "产品编号", "产品名称", "产品价格", "销售数量","合计金额","出售日期","产品种类"};
		// 将列名数组加入到列向量中
		for (int i = 0; i < colum_name.length; i++) {
			
			this.colums.add(colum_name[i]);
		}	
		super.query(sql, paras);
	}
	
	/**
	 * 检查是否存在相关的销售信息
	 * 
	 * @param sql sql语句
	 * 
	 * @param paras 条件数组
	 * 
	 * @return
	 * 		true 存在相关的销售记录，并更新表格模型进行显示<br>
	 * 		false 不存在相关的销售记录
	 */
	public static boolean check(String sql, String []paras) {
		
		boolean b = false;
		SqlHelper sh=new SqlHelper();
		try {
			
			ResultSet rs = sh.query(sql, paras);
			if(rs.next())
			{
				//如果进去，则比较
				if (rs.getInt(1) >= 1) {
					
					b = true;
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			sh.close();
		}
		return b;
	}
	
	/**
	 * 用于得到销售信息表存在的年份和月份
	 * 
	 * @param sql sql语句
	 * 
	 * @param paras 条件数组
	 * 
	 * @return
	 * 		返回找到的年份或者是月份
	 */
	public static double find(String sql, String[] paras) {
		
		double i = 0.0;
		SqlHelper sh=new SqlHelper();
		try {
			
			ResultSet rs = sh.query(sql, paras);
			while(rs.next())
			{
				//如果进去，则取出第一个值
				i = rs.getDouble(1);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			sh.close();
		}
		
		return i;
	}
}
