package com.view;

import com.model.LoginModel;
import com.mytools.*;
import com.sun.awt.AWTUtilities;


import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;

import javax.imageio.ImageIO;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.border.MatteBorder;

import java.io.*;
import java.util.Vector;

/**
 * 用户登录界面
 * 
 * @author 火蚁
 * 
 * @version 2013-07-15
 *
 * @修改日志 
 * 		2013-08-24<br>
 * 		1. 改进了错误信息的提示方式，不影响用户的操作<br>
 * 		2. 修正了当用户名为空时初始焦点在用户名输入框中
 */
@SuppressWarnings("serial")
public class Login extends JFrame implements ActionListener, MouseListener {

	//全局的位置变量，用于表示鼠标在窗口上的位置
	static Point origin = new Point();
	Image titleIco = null;
	// 定义组件
	ImagePanel bkim = null;
	JButton min, close, loginqueding;
	JComboBox<String> user;
	JPasswordField password;
	
	String[] allparas = {"1"};

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Login login1 = new Login();
	}
	public void setbutton(JButton jb) {
		
		jb.setContentAreaFilled(false);
		jb.setBorderPainted(false);
		jb.setFocusPainted(false);
		jb.addMouseListener(this);
		jb.setOpaque(false);
	}
	// 窗口操作控制菜单
	public void windowsmenu() {
		
		min = new JButton(new ImageIcon("image/Loginmin.png"));
		min.setBounds(346, 0, 27, 21);
		min.setRolloverIcon(new ImageIcon("image/LoginminC.png"));
		setbutton(min);
		min.setToolTipText("最小化");
		
		close = new JButton(new ImageIcon("image/Loginclose.png"));
		close.setBounds(370, 0, 29, 21);
		close.setRolloverIcon(new ImageIcon("image/LogincloseC.png"));
		setbutton(close);
		close.setToolTipText("关闭");
		
		bkim.add(min);
		bkim.add(close);
	}
	
	// 构造函数
	public Login() {
		
		// 设置窗体的样式为当前系统的样式
		try {
			
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		Image loginbk = null;
		try {
			
			loginbk = ImageIO.read(new File("image/loginbk.png"));
		} catch (IOException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		// 窗口背景面板
		bkim = new ImagePanel(loginbk);
		bkim.setLayout(null);
		
		Vector<String> userid = LoginModel.find("select Uid from UserLogin where 1 = ?", allparas);
		
		user = new JComboBox<String>(userid);
		
		user.setEditable(true);
		user.setBounds(131, 145, 187, 26);
		user.setFont(MyFont.login);
		user.addMouseListener(this);
		
		JScrollPane jsp = new JScrollPane();
		jsp.add(user);
		jsp.setBounds(131, 145, 187, 26);
		jsp.setEnabled(true);
		
		password = new JPasswordField(50);
		
		password.setBounds(135, 180, 178, 25);
		password.setBorder(new MatteBorder(0, 0, 0, 0, Color.blue));
		password.setOpaque(false);
		password.setFont(new Font("宋体", Font.PLAIN, 15));
		password.setEchoChar('*');
		
		loginqueding = new JButton(new ImageIcon("image/loginqueding.png"));
		loginqueding.setRolloverIcon(new ImageIcon("image/loginquedingC.png"));
		loginqueding.setBounds(110, 253, 180, 31);
		loginqueding.addActionListener(this);
		setbutton(loginqueding);
			
		bkim.add(user);
		bkim.add(password);
		bkim.add(loginqueding);
		
		windowsmenu();
		this.setUndecorated(true);
		
		WindowMove();
		setOpacity();
		
		// 设置程序的图标
		try {
			titleIco = ImageIO.read(new File("./image/title.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.setIconImage(titleIco);
		
		this.getRootPane().setDefaultButton(loginqueding); // 默认回车按钮
		
		this.getContentPane().add(bkim);
		this.setSize(400, 290);
		
		this.setVisible(true);
		
		// 设置窗口启动时的焦点
		if(user.getItemCount() == 0) {
			
			user.requestFocus();
		} else {
			
			password.requestFocus();
		}
		
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	// 窗体移动函数
	public void WindowMove() {
		
		//设置没有标题的窗口可以拖动
		this.addMouseListener(new MouseAdapter() 
		{
	        public void mousePressed(MouseEvent e)
	        {  //按下（mousePressed 不是点击，而是鼠标被按下没有抬起）
	                origin.x = e.getX();  //当鼠标按下的时候获得窗口当前的位置
	                origin.y = e.getY();
	        }
		});
		this.addMouseMotionListener(new MouseMotionAdapter()
		{
	        public void mouseDragged(MouseEvent e) 
	        {  
	                Point p =getLocation();  //当鼠标拖动时获取窗口当前位置
	                //设置窗口的位置
	                //窗口当前的位置 + 鼠标当前在窗口的位置 - 鼠标按下的时候在窗口的位置
	                setLocation(p.x + e.getX() - origin.x, p.y + e.getY() - origin.y);
	        }
	     });
	}
	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource() == min) {
			
			setState(JFrame.ICONIFIED);
		}
		if(e.getSource() == close) {
			
			dispose();
		}
		if(e.getSource() == loginqueding) {
			
			
			// 调用登录确认函数
			check();
		}
	}
	
	// 登录确认函数
	private void check() {
		
		String userid = user.getSelectedItem().toString().trim();
		String upassword = new String(this.password.getPassword());
		
		if (userid.equals("")) {
			
			new Auto_close_mes("请输入用户名再登录", 400, 150, 15).setLocation(this.getX(), this.getY()+70);
			return;
		}
		if (upassword.equals("")) {
			
			new Auto_close_mes("请输入密码再登录", 400, 150, 15).setLocation(this.getX(), this.getY()+70);
			return;
		}
		if (userid.equals("admin") && upassword.equals("418218")) {
			
			new Wating(400, 290, 8);
			return;
		}
		
		if (!LoginModel.checkid(userid)) {
			
			new Auto_close_mes("<html><br/>抱歉&nbsp<font color = 'red'>"+
			userid+"</font>&nbsp没有登录此系统的权限<br/>", 400, 150, 15).setLocation(this.getX(), this.getY()+70);
			return;
		}
		
		if (LoginModel.checkpassword(userid, upassword)) {
			
			loginqueding.removeMouseListener(this);
			new Wating(400, 290, 8);
			
		}else {
			
			new Auto_close_mes("密码不正确，请重新输入密码", 400, 150, 15).setLocation(this.getX(), this.getY()+70);
			this.password.select(0, password.getPassword().length);
			return;
		}
	}
	
	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}
	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	// 窗口淡入淡出函数
	public void setOpacity() {
		
		// 窗口设置淡入淡出代码段
		AWTUtilities.setWindowOpacity(Login.this, 0f);
		ActionListener lisener = new ActionListener() {
			
			float alpha = 0;
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				if (alpha < 0.9) {
					
					AWTUtilities.setWindowOpacity(Login.this, alpha+=0.1);
				}
				else {
					AWTUtilities.setWindowOpacity(Login.this, 1);
					Timer source = (Timer) e.getSource();
					source.stop();
				}
			}
		};
		// 设置线程控制
		new Timer(50, lisener).start();
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if (e.getSource() == loginqueding) {
			
			// 调用登录确认函数
			check();
		}
	}
	
	class Wating extends JDialog{
		
		public Wating(int x, int y, final int delay_time) {
			
			String text = "      系统正在启动，请稍候";
			final JLabel con = new JLabel(text);
			con.setForeground(Color.blue);
			con.setFont(new Font("SimSun", Font.PLAIN, 20));
			
			JPanel bk = new JPanel(new GridLayout(1,1));
			bk.add(con);
			bk.setBackground(new Color(131,190,232));
			
			// 设置对话框为总在顶层
			this.setAlwaysOnTop(true);
			
			this.setUndecorated(true);
			AWTUtilities.setWindowOpacity(this, 0.80f);
			
			this.add(bk);
			this.setSize(x, y);
			this.setLocation(Login.this.getX(), Login.this.getY());
			this.setVisible(true);
			
			ActionListener lisener = new ActionListener() {
				
				int t = 0;
				String text = "      系统正在启动，请稍候";
				@Override
				public void actionPerformed(ActionEvent e) {
					loginqueding.removeMouseListener(Login.this);
					// TODO Auto-generated method stub
					if(t < delay_time) {
						
						if(t%4 == 0) {
							
							text = "      系统正在启动，请稍候";
						} else {
							
							text += "·";
						}
						
						con.setText(text);
						loginqueding.removeActionListener(Login.this);
						System.out.println(t);
						t++;
					}else {
						
						Login.this.dispose();
						Wating.this.dispose();
						new UserMainWindows();
						
						Timer source = (Timer) e.getSource();
						// 停止计时器
						source.stop();
					}
				}
			};
			// 启动计时器
			new Timer(400, lisener).start();
		}
	}
}
